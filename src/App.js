
import "./categories.styles.scss";
import {Routes , Route, Outlet } from "react-router-dom";
import Home from "./routes/home/Home";
import Navigation from "./routes/Navigation/Navigation";
import SignIn from "./routes/SignIn/SignIn";

const App = () => {
 const Shop = () => {
  return <h1>Shop</h1>
 }
  return (
  <Routes>
    <Route path="/" element={<Navigation/>}>
    <Route index element={<Home/>}/>
    <Route path="shop" element={<Shop/>}/>
    <Route path="sign-in" element={<SignIn/>}/>
    </Route>
 
  
  </Routes>
)};

export default App;
