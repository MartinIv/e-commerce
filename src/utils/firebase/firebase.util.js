
import { initializeApp } from "firebase/app";
import {signInWithPopup, signInWithRedirect, getAuth, GoogleAuthProvider} from 'firebase/auth'
import {getDoc, setDoc, doc, getFirestore} from 'firebase/firestore'


const firebaseConfig = {
  apiKey: "AIzaSyADYEOwA3G4MYVMtoaTj72iQmPAOYxrsGo",
  authDomain: "online-shop-30fd2.firebaseapp.com",
  projectId: "online-shop-30fd2",
  storageBucket: "online-shop-30fd2.appspot.com",
  messagingSenderId: "430659792996",
  appId: "1:430659792996:web:1c85fc6ee6b3142e23ef71"
};
export const firebaseApp = initializeApp(firebaseConfig);
export const provider = new GoogleAuthProvider()
provider.setCustomParameters({
    prompt:"select_account"
})

export const auth = getAuth()

export const singInWithGooglePopUp = () => signInWithPopup(auth, provider)
export const db = getFirestore()

export const createUserDocumentFromAuth = async (userAuth) => {
const userDocRef = doc(db, 'users', userAuth.uid)
const userSnapshot = await getDoc(userDocRef)
console.log(userSnapshot);

if(!userSnapshot.exists()){
    const {displayName, email} = userAuth
    const createAt= new Date()

    try {
        await setDoc(userDocRef, {
            displayName,
            email,
            createAt
        })
    } catch (error) {
        console.log(error);
    }
}
return userDocRef
}