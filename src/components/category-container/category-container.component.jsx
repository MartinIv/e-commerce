import React from 'react'
import CategoryItem from '../category-item/category-item.component'
import "./category-container.styles.scss"

const CategoryContainer = ({categories}) => {
  return (
    <div className="categories-container" >
    {categories.map((category) =>  <CategoryItem category={category} key={category.id}  />)}
   
    
  </div>
  )
}

export default CategoryContainer