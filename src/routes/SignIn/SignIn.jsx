import React from 'react'
import { singInWithGooglePopUp, createUserDocumentFromAuth } from '../../utils/firebase/firebase.util'

function SignIn() {
    const signInWithPopUp = async () =>{
         const response = await singInWithGooglePopUp()
         createUserDocumentFromAuth(response.user);
    }
  return (
    <div><h1>Sing In</h1>
    <button onClick={signInWithPopUp}>Sing In with google</button>
    </div>
  )
}

export default SignIn