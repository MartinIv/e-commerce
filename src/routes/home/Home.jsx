import { Outlet } from "react-router-dom";
import { v4 } from "uuid";
import CategoryContainer from "../../components/category-container/category-container.component"; 


const Home = () => {


  const categories = [
    {
      id:v4(),
      title:"Hats",
      imageUrl:"https://i.ibb.co/cvpntL1/hats.png"

    },
    {
      id:v4(),
      title:"Jackets",
      imageUrl:"https://i.ibb.co/px2tCc3/jackets.png"
    },
    {
      id:v4(),
      title:"Sneakers",
      imageUrl:"https://i.ibb.co/0jqHpnp/sneakers.png"
    },
    {
      id:v4(),
      title:"Womens",
      imageUrl:"https://i.ibb.co/GCCdy8t/womens.png"
    },  {
      id:v4(),
      title:"Mens",
      imageUrl:"https://i.ibb.co/R70vBrQ/men.png"
    }
  ]
 
  return (
    <div>
    <Outlet/>
 <CategoryContainer categories={categories}/>
 </div>
  );
}

export default Home;
